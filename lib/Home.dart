import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:intl/intl.dart';
import 'package:notes/helper/NoteHelper.dart';
import 'package:notes/models/Note.dart';
import 'package:intl/date_symbol_data_local.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  TextEditingController _titleController = TextEditingController();
  TextEditingController _descriptionController = TextEditingController();
  var _db = NoteHelper();
  List<Note> notes = List<Note>();

  _showNewNoteDialog({Note note}) {
    String textEditing = "";
    if (note == null) {
      _titleController.text = "";
      _descriptionController.text = "";
      textEditing = "New";
    } else {
      _titleController.text = note.title;
      _descriptionController.text = note.description;
      textEditing = "Edit";
    }

    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text("${textEditing} Note"),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                TextField(
                  controller: _titleController,
                  autofocus: true,
                  decoration: InputDecoration(
                      labelText: "Title", hintText: "type it the note title"),
                ),
                TextField(
                  controller: _descriptionController,
                  autofocus: true,
                  decoration: InputDecoration(
                      labelText: "Descripition",
                      hintText: "type it the note info  "),
                )
              ],
            ),
            actions: <Widget>[
              FlatButton(
                  onPressed: () {
                    _titleController.clear();
                    _descriptionController.clear();
                    Navigator.pop(context);
                  },
                  child: Text("Cancel")),
              FlatButton(
                  onPressed: () {
                    _saveNote(editNote: note);
                    Navigator.pop(context);
                  },
                  child: Text("Add"))
            ],
          );
        });
  }

  _saveNote({Note editNote}) async {
    String title = _titleController.text;
    String description = _descriptionController.text;

    if (editNote == null) {
      Note note = Note(title, description, DateTime.now().toString());
      await _db.saveNote(note);
    } else {
      editNote.title = title;
      editNote.description = description;
      editNote.date = DateTime.now().toString();
      await _db.updateNote(editNote);
    }

    _titleController.clear();
    _descriptionController.clear();

    _getNotes();
  }

  _getNotes() async {
    var db_notes = await _db.getAllNotes();
    List<Note> prev_notes = List<Note>();
    for (Map item in db_notes) {
      prev_notes.add(Note.fromMap(item));
    }

    setState(() {
      notes = prev_notes;
    });
  }

  _removeNote(int id) async {
    await _db.removeNote(id);

    _getNotes();
  }

  _dateFromat(String date) {
    initializeDateFormatting("pt_BR");

    var builder = DateFormat.yMMMMd("pt_BR");

    DateTime dateParse = DateTime.parse(date);
    return builder.format(dateParse);
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _getNotes();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Notes"),
        backgroundColor: Color(0xff00aa12),
      ),
      body: Column(
        children: <Widget>[
          Expanded(
            child: ListView.builder(
                itemCount: notes.length,
                itemBuilder: (context, index) {
                  return Card(
                    elevation: 10,
                    child: ListTile(
                      title: Text(notes[index].title),
                      subtitle: Text(
                          "${_dateFromat(notes[index].date)} - ${notes[index].description}"),
                      trailing: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          GestureDetector(
                            onTap: () {
                              _showNewNoteDialog(note: notes[index]);
                            },
                            child: Padding(
                              padding: EdgeInsets.only(right: 16),
                              child: Icon(Icons.edit, color: Colors.green),
                            ),
                          ),
                          GestureDetector(
                            onTap: () {
                              _removeNote(notes[index].id);
                            },
                            child: Padding(
                              padding: EdgeInsets.only(right: 0),
                              child:
                                  Icon(Icons.delete_forever, color: Colors.red),
                            ),
                          ),
                        ],
                      ),
                    ),
                  );
                }),
          )
        ],
      ),
      floatingActionButton: FloatingActionButton(
          backgroundColor: Color(0xff00aa12),
          foregroundColor: Colors.white,
          child: Icon(Icons.add),
          onPressed: () {
            _showNewNoteDialog();
          }),
    );
  }
}
