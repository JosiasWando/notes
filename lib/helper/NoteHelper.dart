import 'dart:async';

import 'package:notes/models/Note.dart';
import 'package:sqflite/sqflite.dart';
import "package:path/path.dart";

class NoteHelper {
  static final tableName = "note";
  static final NoteHelper _noteHelper = NoteHelper._internal();
  Database _db;

  factory NoteHelper() {
    return _noteHelper;
  }

  NoteHelper._internal() {}

  get db async {
    if (_db != null) {
      return _db;
    } else {
      _db = await startDb();
      return _db;
    }
  }

  startDb() async {
    final databasePath = await getDatabasesPath();
    final databaseLocal = await join(databasePath, "notes.db");

    var db = await openDatabase(databaseLocal, version: 1, onCreate: _onCreate);
    return db;
  }

  _onCreate(Database db, int version) async {
    String sql =
        "CREATE TABLE note(id INTEGER PRIMARY KEY AUTOINCREMENT, title VARCHAR, description TEXT, date DATETIME)";
    await db.execute(sql);
  }

  Future<int> saveNote(Note note) async {
    var database = await db;
    int id = await database.insert(tableName, note.toMap());
    return id;
  }

  Future<int> updateNote(Note note) async {
    Database data = await db;
    return await data
        .update(tableName, note.toMap(), where: "id = ?", whereArgs: [note.id]);
  }

  getAllNotes() async {
    Database data = await db;
    String sql = "SELECT * FROM " + tableName + " ORDER BY date DESC";
    List notes = await data.rawQuery(sql);
    return notes;
  }

  Future<int> removeNote(int id) async {
    Database data = await db;
    return await data.delete(tableName, where: "id = ?", whereArgs: [id]);
  }
}
